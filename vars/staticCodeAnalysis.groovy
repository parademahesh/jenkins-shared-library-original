def call(credentialsId){
    withSonarQubeEnv(credentialsId: credentialsId) {
        sh 'mvn clean package sonar:sonar -Dsonar.projectKey=ragavendira_mypro -Dsonar.organization=ragavendira'
    }
}

